import datetime
import boto3

'''
Create an AMI from an instance
'''
ec2 = boto3.client('ec2')

def handler (event, context):
    DateStamp = '{:%d%m%Y}'.format(datetime.datetime.now())
    InstanceId = 'i-0ae2037d4eaa8db23'
    AmiName = ("%s-%s" % (InstanceId, DateStamp))
    try:
        ec2.create_image(InstanceId=InstanceId, Name=AmiName, NoReboot=True)
        print ("%s %s %s %s" % ("Successfully created", AmiName, "from", InstanceId))
    except:
        print ("AMI backup failed")









