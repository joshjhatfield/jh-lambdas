import datetime
import boto3
from datetime import date


'''
clean up AMI's
'''
ec2 = boto3.client('ec2')

def handler(event, context):

    AMIOwner = '609016308128'
    OriginalAMI = '0ae2037d4eaa8db23'
    DaysToKeep = 55

    response = ec2.describe_images(
        Owners=[AMIOwner]
    )
    parsed = response['Images']



    for ImageDesc in parsed:
        ImageId = ImageDesc['Name']
        if OriginalAMI in ImageId:
            AmiDate = datetime.datetime.strptime(ImageId[20:], '%d%m%Y')
            DateStamp = datetime.datetime.now() - datetime.timedelta(days=DaysToKeep)
            if AmiDate < DateStamp and ImageDesc['State'] == 'available':
                try:
                    def removeAMI():
                        ec2.deregister_image(
                            ImageId=ImageDesc['ImageId'],
                            DryRun=False
                        )

                    removeAMI()
                    print ("%s %s %s %s" % ('Removed AMI', ImageDesc['ImageId'], 'from date', ImageDesc['CreationDate']))
                except:
                    print ("%s %s %s %s" % ('Could not perform deregister operation on', ImageDesc['ImageId'], 'State is:', ImageDesc['State']))










